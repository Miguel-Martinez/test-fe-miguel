import {
  Component,
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';

const endpoint = 'https://reqres.in/api/users';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class FormularioComponent {
  public cabeza = 'Formulario usuarios';
  public usuarios = [];
  constructor(private http: HttpClient) {
    this.extraerData();
  }

  extraerData() {
    this.http.get(endpoint).subscribe((data) => {
      this.usuarios.push(data)
    });
  }

  sortNames() {
    this.usuarios[0].data.sort(function (a, b) {
      var nameA = a.first_name.toLowerCase(),
        nameB = b.first_name.toLowerCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
    console.log(this.usuarios[0].data);
  }
}
