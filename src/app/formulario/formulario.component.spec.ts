import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing'
import { FormularioComponent } from './formulario.component';

fdescribe('FormularioComponent', () => {
  

  beforeEach(() => TestBed.configureTestingModule({
      declarations: [ FormularioComponent ],
      imports: [HttpClientModule, RouterTestingModule]
  }));

  it('should be created', () => {
    const app: FormularioComponent = TestBed.get(FormularioComponent);
    expect(app).toBeTruthy();
  });

  it('should load the data into usuarios', () => {
    const service: FormularioComponent = TestBed.get(FormularioComponent);
    expect(service.usuarios).toBeDefined();
  })

  // it(`should have as title 'Formulario'`, async(() => {
  //   const fixture = TestBed.createComponent(FormularioComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('Formulario');
  // }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(FormularioComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Formulario usuarios');
  // }));

});
